<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1;$i<=20;$i++) {
            if($i==1) {
            User::create([
                'username' => 'demo'.$i,
                'email' => 'demo'.$i.'@demo.mkom',
                'api_token' => '10OA97PhrWdRm5J7h8fjoofttf93XCzRRWFt3b9G83KzYLMVjmWp6kjbCmB7wr592vM0ls4qReBBHzZQWv4VGAtEWXsMdz0o9HCMhN',
                'password' => Hash::make('123456')
            ]);
            } else {
                User::create([
                    'username' => 'demo'.$i,
                    'email' => 'demo'.$i.'@demo.mkom',
                    'password' => Hash::make('123456')
                ]);
            }
        }
    }
}
