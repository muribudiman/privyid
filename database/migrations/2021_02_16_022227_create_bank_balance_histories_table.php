<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankBalanceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_balance_histories', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_balance_id');
            $table->bigInteger('balance_before');
            $table->bigInteger('balance_after');
            $table->string('activity');
            $table->enum('type', ['debit', 'kredit']);
            $table->ipAddress('ip');
            $table->string('location', 255);
            $table->bigInteger('user_agent');
            $table->bigInteger('author');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_balance_histories');
    }
}
