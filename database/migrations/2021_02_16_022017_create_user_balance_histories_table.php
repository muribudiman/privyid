<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserBalanceHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_balance_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_balance_id');
            $table->bigInteger('balance_before');
            $table->bigInteger('balance_after');
            $table->string('activity', 255);
            $table->enum('type', ['debit', 'kredit']);
            $table->ipAddress('ip');
            $table->string('location', 255);
            $table->bigInteger('user_agent');
            $table->bigInteger('author');
            $table->timestamps();
            
            $table->foreign('user_balance_id')->references('id')->on('user_balances')->onDelete('cascade');
            
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_balance_histories');
    }
}
