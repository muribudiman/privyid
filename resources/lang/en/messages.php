<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Messages Language Lines
    |--------------------------------------------------------------------------
    */

    'message_update' => 'The :attribute successfully updated',
    'message_create' => 'The :attribute successfully created',
    'message_delete' => 'The :attribute delete successfully',
    'message_assign' => ':attribute assigned to an :name',
    'message_glob' => ':attribute successfully :name',
    'message_not_found' => 'data not found',
    'message_notification' => 'You have a new reply on this ticket with id ":attribute" ',
    
    
    /*
    |--------------------------------------------------------------------------
    |   Change Password
    |--------------------------------------------------------------------------
     */
    
    'message_incorrect_old_password' => 'Password was not updated. Incorrect old password',
    
    /*
    |--------------------------------------------------------------------------
    |   Api Message
    |--------------------------------------------------------------------------
     */
    
    'api_unauthenticated' => 'Unauthenticated',
    'api_login' => 'You logged in',
    'api_logout' => 'you\'re out',
];
