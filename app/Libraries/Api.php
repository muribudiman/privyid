<?php

namespace App\Libraries;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Api
 *
 * @author muri
 */
class Api {
    
    public function __construct() {
        
    }
    
    /**
     * 
     * @param type $success
     * @param type $error
     * @param type $message
     * @param type $data
     * @return type array
     */
    public static function message($success = false, $error = [], $message = [], $data = [], $pagination = []) {
        $errorData = [];
        foreach($error as $key => $value) {
            $errorData[$key] = $value;
        }
        
        return [
            'success' => (bool) $success,
            'error' => (array) $errorData,
            'message' => (array) $message,
            'data' => (array) $data,
            'pagination' => (array) $pagination,
        ];
    }
    
}
