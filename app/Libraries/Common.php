<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Libraries;


use App\Models\UserBalance;
use App\Models\UserBalanceHistory;
use App\Models\User;
/**
 * Description of Common
 *
 * @author muri
 */
class Common {
    
    static $model;
    static $ip;
    static $type;
    static $user_agent;
    static $user_id;
    
    public function __construct($model, $ip, $type, $user_agent, $user_id) {
        self::$model = $model;
        self::$ip = $ip;
        self::$type = $type;
        self::$user_agent = $user_agent;
        self::$user_id = $user_id;
    }
    
    public static function makeHistory() {
        $find = UserBalanceHistory::leftJoin('user_balances', 'user_balances.id', '=', 'user_balance_histories.user_balance_id')
                    ->where('user_balances.user_id', self::$user_id)
                    ->orderBy('user_balance_histories.id', 'DESC')->first();
        
        if($find) {
            $balance_before = $find->balance_after;
            if(self::$type == 'kredit') {
                $balance_after = $find->balance_after + self::$model->balance_achieve;
            } else {
                $balance_after = $find->balance_after - self::$model->balance_achieve;
            }
        } else {
            $balance_before = 0;
            $balance_after = self::$model->balance_achieve ;
        }
        
        User::where('id', self::$user_id)->update(['balance' => $balance_after]);
        
        
        $myType = self::$type == 'transfer' ? 'debit' : self::$type;
        UserBalanceHistory::create([
            'user_balance_id' => self::$model->id,
            'balance_before' => $balance_before,
            'balance_after' => $balance_after,
            'activity' => self::$type,
            'type' => $myType,
            'ip' => self::$ip,
            'location' => "null",
            'user_agent' => self::$user_agent,
            'author' => self::$user_id,
        ]);
        
    }
    
    public static function makeTransfer() {
        if(self::$type == 'transfer') {
            $model = new UserBalance;
            $model->user_id = self::$user_agent;
            $model->balance = self::$model->balance;
            $model->balance_achieve = self::$model->balance_achieve;
            if($model->save()) {
                $find = UserBalanceHistory::leftJoin('user_balances', 'user_balances.id', '=', 'user_balance_histories.user_balance_id')
                    ->where('user_balances.user_id', $model->user_id)
                    ->orderBy('user_balance_histories.id', 'DESC')->first();
        
                if($find) {
                    $balance_before = $find->balance_after;
                    $balance_after = $find->balance_after + $model->balance_achieve;
                } else {
                    $balance_before = 0;
                    $balance_after = $model->balance_achieve ;
                }

                User::where('id', $model->user_id)->update(['balance' => $balance_after]);

                UserBalanceHistory::create([
                    'user_balance_id' => $model->id,
                    'balance_before' => $balance_before,
                    'balance_after' => $balance_after,
                    'activity' => self::$type,
                    'type' => 'kredit',
                    'ip' => self::$ip,
                    'location' => "null",
                    'user_agent' => self::$user_agent,
                    'author' => self::$user_id,
                ]);
            }
        }
    }
    
}
