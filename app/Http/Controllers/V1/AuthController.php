<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use App\Libraries\Api;
use App\Models\User;

class AuthController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware("auth");
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'username' => 'required',
                'password' => 'required',
            ]);
            
            if ($validator->fails()) {
                $error = Api::message(false, [$validator->errors()->toArray()], [], []);
                return response()->json($error, 200);
            } else {
                $model = User::where(function($query) use($request) {
                    $query->where('username', $request->input('username'))
                            ->orWhere('email', $request->input('username'));
                })->first();
                
                if($model && Hash::check($request->input('password'), $model['password'])) {
                    if($model->api_token == null) {
                        $model->generateToken();
                    }
                    
                    $model->token = $model->api_token;
                    
                    $success = Api::message(true, [], [["msg" => [Lang::get('messages.api_login')]]], [$model->toArray()]);
                    return response()->json($success, 200);
                } 
                
                $errorlogin = [
                    "email" => [
                        0 => "This credential does not match our records."
                    ]
                ]; 
                
                $success = Api::message(false, [$errorlogin], [], []);
                return response()->json($success, 200);
            }
        } catch (\Exception $ex) {
            $success = Api::message(false, [["message" => [$ex->getMessage()]]], [], [], []);
            return response()->json($success, 200);
        }
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        try {
            $auth = \Auth::user();
            $model = User::find($auth->id);
            $model->api_token = null;
            $model->save();
            $success = Api::message(true, [], [["msg" => [Lang::get('messages.api_logout')]]], [$model]);
            return response()->json($success, 200);
        } catch (\Exception $ex) {
            $success = Api::message(false, [["message" => [$ex->getMessage()]]], [], [], []);
            return response()->json($success, 200);
        }
    }

   
}
