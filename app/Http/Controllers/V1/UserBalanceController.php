<?php

namespace App\Http\Controllers\V1;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Lang;
use Illuminate\Http\Request;
use App\Libraries\Api;
use App\Libraries\Common;
use App\Models\UserBalance;

class UserBalanceController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("auth");
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        try {
            $auth = \Auth::user();
        
            $page = 1;

            $query = UserBalance::leftJoin('user_balance_histories', 'user_balance_histories.user_balance_id', '=', 'user_balances.id')
                    ->leftJoin('users as author', 'author.id', '=', 'user_balance_histories.author')
                    ->leftJoin('users as agent', 'agent.id', '=', 'user_balance_histories.user_agent')
                    ->where('user_balances.user_id', $auth->id)
                    ->orderBy('user_balances.id', 'ASC');

            $model = $query->select(
                        'user_balances.balance_achieve as balance_achieve',
                        'user_balance_histories.balance_before as balance_before',
                        'user_balance_histories.balance_after as balance_after',
                        'user_balance_histories.activity as activity',
                        'user_balance_histories.type as type',
                        'user_balance_histories.ip as ip',
                        'author.username as author_name',
                        'author.username as agent_name'
                    )
                    ->paginate($page)
                    ->appends(request()->query());
                     
            $paginate = [
                'total' => (int) $model->total(),
                'currentPage' => (int) $model->currentPage(),
                'lastPage' => (int) $model->lastPage(),
                'hasMorePages' => (boolean) $model->hasMorePages(),
                'perPage' => (int) $model->perPage(),
                'total' => (int) $model->total(),
                'lastItem' => (int) $model->lastItem(),
            ];
            
            $success = Api::message(true, [], [["msg" => ["tickets"]]], $model->items(), [$paginate]);
            return response()->json($success, 200);
            
        } catch (\Exception $ex) {
            $success = Api::message(false, [["message" => [$ex->getMessage()]]], [], [], []);
            return response()->json($success, 200);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $type)
    {
                    
        try {
            $auth = \Auth::user();
            $arrayType = ['kredit', 'debit', 'transfer'];
            
            if($type == 'kredit' || $type == 'debit') {
                $validator = Validator::make($request->all(), [
                    "balance" => "required|numeric",
                    "balance_achieve" => "required|numeric|min:1",
                ]);
                $user_agent = $auth->id;
            } else {
                $validator = Validator::make($request->all(), [
                    "user_id" => "required|numeric|exists:users,id",
                    "balance" => "required|numeric",
                    "balance_achieve" => "required|numeric|min:1",
                ]);
                
                $user_agent = $request->input('user_id');
            }
            
            
            if ($validator->fails()) {
                $error = Api::message(false, [$validator->errors()->toArray()], [], []);
                return response()->json($error, 200);
            } else if(!in_array($type, $arrayType)) {
                $error = Api::message(false, [["error" => ["wrong type"]]], [], []);
                return response()->json($error, 200);
            } else if(($type == 'debit' || $type == 'transfer') && (int) $auth->balance < (int) $request->input('balance_achieve')) {
                $error = Api::message(false, [["error" => ["balance insufficient balance"]]], [], []);
                return response()->json($error, 200);
            } else {
                $model = new UserBalance;
                $model->user_id = $auth->id;
                $model->balance = $request->input('balance');
                $model->balance_achieve = $request->input('balance_achieve');
                if($model->save()) {
                    $common = new Common($model, $request->getClientIp(), strtolower($type), $user_agent, $auth->id, $request->input('activity'));
                    $common::makeHistory();
                    $common::makeTransfer();
                }
                
                $success = Api::message(true, [], [["msg" => [Lang::get('messages.message_create', ['attribute' => $type])]]], [], []);
                return response()->json($success, 200);
            }
            
        } catch (\Exception $ex) {
            $success = Api::message(false, [["message" => [$ex->getMessage()]]], [], [], []);
            return response()->json($success, 200);
        }
    }

}
