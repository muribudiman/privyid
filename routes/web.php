<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api'], function () use ($router) {
    $router->group(['prefix' => 'v1'], function () use ($router) {
        $router->get('/', function () use ($router) {
            return ['privyid' => 'v1'];
        });
        
        /** 
         * AuthController
         */
        $router->post('login', 'V1\AuthController@login');
        $router->group(['middleware' => 'auth'], function () use ($router) {
            $router->post('logout', 'V1\AuthController@logout');
        });
        
        /*
         * UserBalanceController
         */
        $router->get('balance/index', 'V1\UserBalanceController@index');
        $router->post('balance/create/{type:[a-z]+}', 'V1\UserBalanceController@create');   
        
    });
});
