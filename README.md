# Lumen PHP Framework

[![Build Status](https://travis-ci.org/laravel/lumen-framework.svg)](https://travis-ci.org/laravel/lumen-framework)
[![Total Downloads](https://img.shields.io/packagist/dt/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![Latest Stable Version](https://img.shields.io/packagist/v/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)
[![License](https://img.shields.io/packagist/l/laravel/framework)](https://packagist.org/packages/laravel/lumen-framework)

Laravel Lumen is a stunningly fast PHP micro-framework for building web applications with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Lumen attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as routing, database abstraction, queueing, and caching.

## Install
    - composer install

## Database Config
```
    edit .env
    
    DB_CONNECTION=mysql
    DB_HOST=127.0.0.1
    DB_PORT=3306
    DB_DATABASE=lumen_privyid
    DB_USERNAME=root
    DB_PASSWORD=123456
```

## Database
    - create database lumen_privyid
    - php artisan migrate

## Running Seeders
    -- Now you may use the db:seed Artisan command to seed your database. By default, the db:seed command runs the DatabaseSeeder class, which may be used to call other seed classes. However, you may use the --class option to specify a specific seeder class to run individually:
    - php artisan db:seed
    -- You may also seed your database using the migrate:refresh command, which will also rollback and re-run all of your migrations. This command is useful for completely re-building your database:
    - php artisan migrate:refresh --seed



### URL
* {{url}} = http://api.privyid.mkom
* Auth Login:
    * POST {{url}}/api/v1/login
    * Body Json
    {
    "username" : "demo10@demo.mkom",
        "password" : "123456"
    }

* Auth Logout:
    * POST {{url}}/api/v1/logout
    * Autorization : Bearer Token

* User Balance Kredit:
    * POST {{url}}/api/v1/balance/create/kredit
    * Autorization : Bearer Token
    * Body Json
    {
        "balance" : 0,
        "balance_achieve" : 12000000
    }

* User Balance Debit:
    * POST {{url}}/api/v1/balance/create/debit
    * Autorization : Bearer Token
    * Body Json
    {
        "balance" : 0,
        "balance_achieve" : 100000
    }

* User Balance Transfer:
    * POST {{url}}/api/v1/balance/create/transfer
    * Autorization : Bearer Token
    * Body Json
    {
        "user_id" : 2,
        "balance" : 0,
        "balance_achieve" : 2100000
    }

* List User Balance:
    * GET {{url}}/api/v1/balance/index?page=1
    * Autorization : Bearer Token


